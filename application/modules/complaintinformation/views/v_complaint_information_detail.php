
	<!--================ Start Element Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
			<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
			<div class="container">
				<div class="banner_content text-left">
					<div class="page_link">
						<a href="<?php echo base_url('home'); ?>">Home</a>
						<a href="<?php echo base_url('complaintinformation'); ?>">Keluhan</a>
						<a href="<?php echo base_url('complaintinformation/detail/').$this->uri->segment(3); ?>">Detail Keluhan</a>
					</div>
					<h2>Detail Keluhan Anda</h2>
				</div>
			</div>
		</div>
	</section>
	<!--================ End Features Banner Area =================-->

	<!--================ Page Content ================-->
	<div class="whole-wrap">
		<div class="container">
			<div class="section-top-border" id="form">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<h3 class="mb-30 title_color">Detail Keluhan</h3>
					</div>
					<div class="col-lg-4">
						<div class="card-body">
							<div class="row form-group">
								<img src="<?php echo base_url(); ?>assets/upload_img/<?php echo $photo_file; ?>" class="img-rounded img-thumbnail" alt="">
							</div>
						</div>
					</div>
					<div class="col-lg-8">
						<div class="card-body">
							<div class="row form-group">
                                <div class="col col-md-2">
                                	<label class="form-control-label">Nama Pelapor</label>
                                </div>
                                <div class="col-12 col-md-10">
                                    <label class="form-control-label">: <?php echo $name; ?></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                	<label class="form-control-label">Lokasi</label>
                                </div>
                                <div class="col-12 col-md-10">
                                    <label class="form-control-label">: <?php echo $location; ?></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                	<label class="form-control-label">Pesan</label>
                                </div>
                                <div class="col-12 col-md-10">
                                    <label class="form-control-label">: <?php echo $message; ?></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                	<label class="form-control-label">Tanggal</label>
                                </div>
                                <div class="col-12 col-md-10">
                                    <label class="form-control-label">: <?php echo $date; ?></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                	<label class="form-control-label">Status</label>
                                </div>
                                <div class="col-12 col-md-10">
                                    <label class="form-control-label">: <?php echo $status_name; ?></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                	<label class="form-control-label">Tanggapan</label>
                                </div>
                                <div class="col-12 col-md-10">
                                    <label class="form-control-label">: <?php echo $response; ?></label>
                                </div>
                            </div>
						</div>
					</div>
			    </div>
			</div>
		</div>
	</div>
	<!--================ End Page Content ================-->