<!doctype html>
<head>
    <!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.png" type="image/png">
    <title><?php echo isset($company) ? $company : 'Kominfo'; ?> | <?php echo isset($title) ? $title : 'Home'; ?></title>
    
    <!-- plugins:css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
	<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/bootstrap/js/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datepicker/bootstrap-datepicker.min.css"/>

	<style>
    	body { background-image: url("<?php echo base_url(); ?>assets/img/body-bg.png"); }
  	</style>
</head>
<body>