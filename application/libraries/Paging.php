<?php

class Paging {

	function createPagination($url, $per_page, $total)
	{
		$ci =& get_instance();
		$ci->load->library('pagination');

		$config['base_url']         = base_url().$url;
		$config['total_rows']       = $total;
		$config['per_page']         = $per_page;
		$config["uri_segment"]      = 3;
		$choice                     = $config["total_rows"] / $config["per_page"];
		$config["num_links"]        = floor($choice);

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		
		$config['full_tag_open']    = '<div class="pagging text-center"><nav aria-label="Page navigation example"><ul class="pagination justify-content-end">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span><span class="sr-only">Next</span></li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';

		$ci->pagination->initialize($config);

		$pagination = $ci->pagination->create_links();

		return $pagination;
	}

}

?>