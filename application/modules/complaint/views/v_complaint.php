
	<style>
        div.preview {max-width:250px;height:150px;border:2px solid #8252fa; border-radius: 6px;}
        img.preview {width:100%;height:100%;}
    </style>

	<!--================ Start Element Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
			<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
			<div class="container">
				<div class="banner_content text-left">
					<div class="page_link">
						<a href="<?php echo base_url('home'); ?>">Home</a>
						<a href="<?php echo base_url('complaint'); ?>">Keluhan</a>
					</div>
					<h2>Input Keluhan Anda</h2>
				</div>
			</div>
		</div>
	</section>
	<!--================ End Features Banner Area =================-->

	<!--================ Page Content ================-->
	<div class="whole-wrap">
		<div class="container">
			<div class="section-top-border" id="form">
			<form method="post" role="form" id="form_input" enctype="multipart/form-data" action="<?php echo base_url('complaint/save'); ?>">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<h3 class="mb-30 title_color">Form Keluhan</h3>
					</div>
					<div class="col-lg-12 col-md-12">
						<div class="alert alert-success alert-white rounded" style="display: none;">
		                    <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
		                    <div class="icon">
		                        <i class="fa fa-check"></i>
		                    </div>
		                    <div class="message" style="padding-left:40px;">
		                    	
		                    </div>
		                </div>
		                <div class="alert alert-danger alert-white rounded" style="display: none;">
		                    <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
		                    <div class="icon">
		                        <i class="fa fa-times-circle"></i>
		                    </div>
		                    <div class="message" style="padding-left:40px;">
		                        
		                    </div>
		                </div>
	                </div>
					<div class="col-lg-3 col-md-3">
						<div class="mt-10">
							<div class="preview">
								<img class="preview" id="prevImagePhoto" src="<?php echo base_url(); ?>assets/img/preview.png" alt="Preview Image People" />
							</div><br>
							<input type="file" class="form-control-file" name="photo" onchange="readURLPhoto(this)">
						</div>
					</div>
					<div class="col-lg-9 col-md-9">
						<div class="mt-10">
							<input type="text" name="name" placeholder="Nama Pelapor" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Pelapor'" class="single-input">
						</div>
						<div class="mt-10">
							<input type="text" name="location" placeholder="Lokasi" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Lokasi'" class="single-input">
						</div>
						<div class="mt-10">
							<textarea name="message" class="single-textarea" placeholder="Pesan" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Pesan'" ></textarea>
						</div>
					</div>
					<div class="col-lg-12 col-md-12">
						<div class="mt-10 text-right">
							<br>
							<button class="genric-btn primary"><span>Simpan</span></button>
							<!-- <input type="submit" class="genric-btn primary" id="simpan" value="Simpan"> -->
						</div>
					</div>
			    </div>
			</form>
			</div>
		</div>
	</div>
	<!--================ End Page Content ================-->

	<script type="text/javascript">
        function readURLPhoto(input) {
            if (input.files && input.files[0]) {   
                var reader = new FileReader();
                reader.onload = function (e)
                {document.getElementById('prevImagePhoto').src=e.target.result;}
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#form_input').on('submit',(function(e) {

        	e.preventDefault();
            var formData = new FormData(this);

            $.ajax({
                type        : 'POST',
                url         : $(this).attr('action'),
                data        : formData,
                cache       : false,
                contentType : false,
                processData : false,
                success     : function(data){

                    if((data != '') && (data != null)) {
                        if (typeof data !='object') { data = $.parseJSON(data); }

                        if (data.result == 'success') {
                        	$('.alert-success .message').append('<p class="mb-0">'+data.message+'</p>');
                            $('.alert-success').show();
                            $('.alert-danger').hide();
                            $('#form :input').val('');
                            document.getElementById('prevImagePhoto').src='<?php echo base_url(); ?>assets/img/preview.png';
                            document.documentElement.scrollTop = 50;
                        } else {
                        	$('.alert-danger .message').html('');

                            if (typeof data.message == 'object') {
                                $('.alert-danger .message').append('<ul></ul>');
                                $.each(data.message, function(index, error) {
                                    $('.alert-danger .message ul').append('<li>'+error+'</li>');
                                });
                            } else {
                                $('.alert-danger .message').append('<p class="mb-0">'+data.message+'</p>');
                            }
                            $('.alert-success').hide();
                            $('.alert-danger').show();
                            document.documentElement.scrollTop = 200;
                        }
                    }

                },
            });
        }));
    </script>