<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_info_complaint extends CI_Model {
 
    function __construct ()
    {
    	parent::__construct();
        $this->table_name = 'complaint';
        $this->table_ref  = 'ref_status_complaint';
    }

    function totalData()
    {
        return $this->db->get($this->table_name)->num_rows();
    }

    function getData($number, $offset, $message)
    {
        $this->db->select('
                    complaint_id AS `id`,
                    complaint_reporter AS `name`,
                    complaint_report AS `message`,
                    complaint_location AS `location`,
                    complaint_date_report AS `date`,
                    complaint_status AS `status`,
                    status_name AS `status_name`,
                    complaint_response AS `response`,
                    complaint_photo_file AS `photo_file`,
                    complaint_photo_path AS `photo_path`,
                    complaint_photo_type AS `photo_type`,
                    complaint_photo_size AS `photo_size`
                ');
        $this->db->from($this->table_name);
        $this->db->join($this->table_ref, ''.$this->table_name.'.complaint_status = '.$this->table_ref.'.status_id', 'left');
        if ($message != '') {
            $this->db->like('complaint_report', $message);
        }
        $this->db->order_by('complaint_id', 'DESC');
        $this->db->limit($number, $offset);
        return $this->db->get()->result_array();
    }

    function getDataDetail($id)
    {
        $this->db->select('
                    complaint_id AS `id`,
                    complaint_reporter AS `name`,
                    complaint_report AS `message`,
                    complaint_location AS `location`,
                    complaint_date_report AS `date`,
                    complaint_status AS `status`,
                    status_name AS `status_name`,
                    complaint_response AS `response`,
                    complaint_photo_file AS `photo_file`,
                    complaint_photo_path AS `photo_path`,
                    complaint_photo_type AS `photo_type`,
                    complaint_photo_size AS `photo_size`
                ');
        $this->db->from($this->table_name);
        $this->db->join($this->table_ref, ''.$this->table_name.'.complaint_status = '.$this->table_ref.'.status_id', 'left');
        $this->db->where('complaint_id', ''.$id.'');
                
        return $this->db->get()->row_array();
    }

}