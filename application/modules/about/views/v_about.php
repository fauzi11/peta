	
    <div class="container" style="margin-top:6%;">
        <div class="jumbotron text-center" style="margin-bottom:0;background-color:#A9A9A9;">
            <div class="row">
                <div class="col-sm-12">
                    <div id="carouselIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-10 mx-auto" src="assets/img/the-image-slide-green.png" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-10 mx-auto" src="assets/img/the-image-slide-red.png" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-10 mx-auto" src="assets/img/the-image-slide-violet.png" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
