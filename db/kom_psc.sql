/*
SQLyog Ultimate v11.2 (64 bit)
MySQL - 10.1.9-MariaDB : Database - kom_psc
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`kom_psc` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `kom_psc`;

/*Table structure for table `complaint` */

DROP TABLE IF EXISTS `complaint`;

CREATE TABLE `complaint` (
  `complaint_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `complaint_reporter` varchar(50) DEFAULT NULL COMMENT 'pelapor',
  `complaint_report` text COMMENT 'laporan pengaduan',
  `complaint_location` varchar(100) DEFAULT NULL COMMENT 'lokasi',
  `complaint_date_report` date DEFAULT NULL COMMENT 'tanggal laporan pengaduan',
  `complaint_status` tinyint(2) DEFAULT NULL COMMENT 'status laporan pengaduan (ref to ref_status_complaint)',
  `complaint_response` text COMMENT 'penanggapan laporan pengaduan',
  `complaint_photo_file` varchar(255) DEFAULT NULL COMMENT 'nama file gambar',
  `complaint_photo_path` varchar(255) DEFAULT NULL COMMENT 'lokasi file gambar',
  `complaint_photo_type` varchar(50) DEFAULT NULL COMMENT 'tipe gambar',
  `complaint_photo_size` int(11) DEFAULT NULL COMMENT 'ukuran gambar',
  PRIMARY KEY (`complaint_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `complaint` */

insert  into `complaint`(`complaint_id`,`complaint_reporter`,`complaint_report`,`complaint_location`,`complaint_date_report`,`complaint_status`,`complaint_response`,`complaint_photo_file`,`complaint_photo_path`,`complaint_photo_type`,`complaint_photo_size`) values (1,'Samidi','Lampu bangjo error','Perempatan sayidan','2019-02-14',1,NULL,'sdfsfsdfsdfsdfsd.png','C:/xampp/htdocs/kominfo/psc_/assets/upload_img/','png',102),(2,'Paijan','Jalan berlubang lubang','Jl Kusuma Negara No 120','2019-02-14',1,NULL,'sdfsdfsdfsdgsdfsd.png','C:/xampp/htdocs/kominfo/psc_/assets/upload_img/','png',88),(3,'Bopak Castillo','Penertibpan pohon pohon yang dahannya di atas kabel, yang kemungkinan jika hujan lebat dan berangin akan mengakibatkan kabel putus.','Daerah kebun binatang','2019-02-14',1,NULL,'4d72dd7e2343595b27d9817a819d9dcc.jpg','C:/xampp/htdocs/kominfo/psc_/assets/upload_img/','jpeg',45),(4,'Daus','Banyak sampah dipinggir pinggir jalan, mohon ditertibkan dan disediakan tempat sampah','Jl Malioboro','2019-02-15',1,NULL,'789fcfa65b52579e4608733cb2f95980.png','C:/xampp/htdocs/kominfo/psc_/assets/upload_img/','png',8),(5,'Agung','Pencemaran nama baik','Dinas Kominfo','2019-02-15',1,NULL,'asfsfsdfdsfsdf66346.png','C:/xampp/htdocs/kominfo/psc_/assets/upload_img/','png',345);

/*Table structure for table `ref_company` */

DROP TABLE IF EXISTS `ref_company`;

CREATE TABLE `ref_company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(100) DEFAULT NULL,
  `company_email` varchar(100) DEFAULT NULL,
  `company_phone` varchar(50) DEFAULT NULL,
  `company_address` text,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `ref_company` */

insert  into `ref_company`(`company_id`,`company_name`,`company_email`,`company_phone`,`company_address`) values (1,'PSC','psc@jogjakota.com','+6285643011222','Kota Yogyakarta');

/*Table structure for table `ref_question_category` */

DROP TABLE IF EXISTS `ref_question_category`;

CREATE TABLE `ref_question_category` (
  `question_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_category_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`question_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `ref_question_category` */

insert  into `ref_question_category`(`question_category_id`,`question_category_name`) values (1,'Kondisi Jalan'),(2,'Infrastruktur Jalan'),(3,'Rambu Lalu Lintas');

/*Table structure for table `ref_status_complaint` */

DROP TABLE IF EXISTS `ref_status_complaint`;

CREATE TABLE `ref_status_complaint` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `ref_status_complaint` */

insert  into `ref_status_complaint`(`status_id`,`status_name`) values (1,'Belum Ditanggapi'),(2,'Sudah Ditanggapi'),(3,'Proses Ditanggapi');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
