
	<!--================ Start Element Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
			<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
			<div class="container">
				<div class="banner_content text-left">
					<div class="page_link">
						<a href="<?php echo base_url('home'); ?>">Home</a>
						<a href="<?php echo base_url('question'); ?>">Pertanyaan & Saran</a>
					</div>
					<h2>Laporan Pertanyaan & Saran</h2>
				</div>
			</div>
		</div>
	</section>
	<!--================ End Features Banner Area =================-->

	<!--================ Page Content ================-->
	<div class="whole-wrap">
		<div class="container">
			<div class="section-top-border">
				<div class="progress-table-wrap">
					<div class="progress-table">
						<div class="row">
					      <div class="col-lg-12">
					        <h2>Pertanyaan & Saran</h2>
					      </div>
					      <div class="col-lg-12">
					      	
					      </div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--================ End Page Content ================-->