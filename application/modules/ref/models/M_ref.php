<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_ref extends CI_Model {
 
    function __construct ()
    {
    	parent::__construct();
    }

    function getCompany()
    {
        $this->db->select('
                            company_id AS `id`,
                            company_name AS `name`,
                            company_phone AS `phone`,
                            company_address AS `address`
                        ');
        $this->db->from('ref_company');
        return $this->db->get()->result_array();
    }

}